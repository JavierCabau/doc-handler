from pyexcel_ods import get_data
import pyexcel as pe
import csv
import numpy as np
import pandas as pd
import os


def import_ods(filedir, sheetname='pyexcel sheet'):
    """
    Import ods files
    :param filedir: path to file
    :param sheetname: name of sheet
    :return: the parsed array
    """
    dict_array = get_data(filedir)
    try:
        array = dict_array[sheetname]
    except:
        array = dict_array['Sheet1']
    return array


def save_ods(file, name):
    """
    save the array into a .ods file
    :param file: array to save
    :param name: name/path of final .ods file
    :return: None
    """
    try:
        sheet = pe.Sheet(file)
        file_name = name
        sheet.save_as(file_name)
    except:
        if isinstance(file[0], str):
            for i in range(len(file)):
                file[i] = [file[i]]
            sheet = pe.Sheet(file)
            file_name = name
            sheet.save_as(file_name)
        else:
            new = []
            for i in range(len(file)):
                row = []
                for j in range(len(file[i])):
                    row.append(str(file[i][j]))
                new.append(row)
            sheet = pe.Sheet(new)
            file_name = name
            sheet.save_as(file_name)

    return


def save_csv(variable, name, separator='|'):
    """
    save a array to .csv file
    :param variable: the array to save
    :param name: name/path of final .csv file
    :param separator: separator to make the .csv (default='|' char)
    :return:
    """
    if isinstance(variable, np.ndarray):
        variable1 = list(map(lambda a: list(a), variable))
    else:
        variable1 = variable
    with open(name, mode='w', encoding='utf-8') as x:
        y = csv.writer(x, delimiter=separator)
        y.writerow(list(map(lambda a: a, variable1)))
    return


def import_csv(filedir, separator='|'):
    """
    import a .csv file to a array
    :param filedir: path or name of the .csv file
    :param separator: separator to parse the .csv (default='|' char)
    :return: array
    """
    with open(filedir, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=separator)
        readed = list(map(lambda a: eval(a), csv_reader.fieldnames))

    return readed


def save_txt(file, name):
    """
    save a .txt document
    :param file: array to save
    :param name: name w/wo path to finale document
    :return:None
    """
    f = open(name, 'w')
    for i in range(len(file)):
        f.write(file[i])
    return


def import_txt(file):
    with open(file, mode='r') as txt:
        listed = txt.readlines()
    return listed


def write_ods(output_file, row):
    """
    :param output_file: must be the name of the .ods file where you want to write.
    :param row: list
    :return: None
    """
    if not os.path.isfile(output_file):
        save_ods([], output_file)
    sheet = pe.get_sheet(file_name=output_file)
    final_row = []
    if isinstance(row, list):
        for r in range(len(row)):
            if isinstance(row[r], list):
                final_row.append(str(row[r]))
            else:
                final_row.append(row[r])
    else:
        final_row = row
    sheet.row += final_row
    sheet.save_as(output_file)
    return


def read_csvRMA(path, indexes=False):
    """
    Read a RMA .csv output
    :param path:
    :return: matrix data python like
    """
    data_0 = pd.read_csv(path)
    data_1 = list(map(lambda a: list(a)[0].split(';'), list(data_0.values)))
    if indexes:
        process_columns_raw = list(map(lambda a: a.split('"'), list(data_0.columns)[0].split(';')))
        process_columns = []
        for ijk in range(len(process_columns_raw)):
            if ijk > 0:
                process_columns.append(process_columns_raw[ijk][1].strip())
            else:
                process_columns.append(process_columns_raw[ijk])

        final_resultant_matrix = [process_columns] + data_1
    else:
        final_resultant_matrix = list(map(lambda a: a[1:], data_1))

    return final_resultant_matrix
